import React from 'react';
import './App.scss';
import Card from './components/card';
import 'normalize.css';
import './fonts/Exo2.0-Thin.otf';
import './fonts/trebushetms.ttf';

const products = [
  {
    productName: 'Нямушка',
    flavour: 'с фуа-гра',
    tagline: 'Сказочное заморской яство',
    weight: '0,5',
    promoPortions: '10',
    promoGifts: '',
    promoDeclination: 'мышь',
    promoComments: '',
    additionalText: 'Печень утки разварная с артишоками.',
    isDisabled: false
  },
  {
    productName: 'Нямушка',
    flavour: 'с рыбой',
    tagline: 'Сказочное заморской яство',
    weight: '2',
    promoPortions: '40',
    promoGifts: '2',
    promoDeclination: 'мыши',
    promoComments: '',
    additionalText: 'Головы щучьи с чесноком да свежайшая сёмгушка.',
    isDisabled: false
  },
  {
    productName: 'Нямушка',
    flavour: 'с курой',
    tagline: 'Сказочное заморской яство',
    weight: '5',
    promoPortions: '100',
    promoGifts: '5',
    promoDeclination: 'мышей',
    promoComments: 'заказчик доволен',
    additionalText: 'Филе цыплят с трюфелями в бульоне.',
    isDisabled: true
  }
];

function App() {
  
  return (
    <div className="App">
      <h1>Ты сегодня покормил кота?</h1>
      <div className='products-box'>
        {products.map(product => <Card key={product.flavour.toString()} product={product} />)}
      </div>
    </div>
  );
}

export default App;
