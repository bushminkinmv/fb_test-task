import React from 'react';
import bgImg from '../../img/kitty.png';
import '../../styles/main.scss';
import './styles.scss';

class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      isSelected: false,
      isLeaved: false,
      hoveredText: false
    };
  }

  hoverHandler = () => {
    if (this.state.isLeaved && this.state.isSelected) {
      this.setState(() => ({ hoveredText: true }));
    }
  }

  leaveHandler = () => {
    this.setState(() => ({ 
      isLeaved: true,
      hoveredText: false
    }));
  }

  selectProduct = () => {
    if (!this.props.product.isDisabled) {
      this.setState(state => ({ 
        isSelected: !state.isSelected,
        hoveredText: false
      }));
    }
  }

  render() {
    return (
      <div className='card'>
        <div className='card__box' onClick={this.selectProduct} onMouseLeave={this.leaveHandler} onMouseEnter={this.hoverHandler}>
          <svg width="320px" height="480px" viewBox="0 0 320 480" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <defs>
                <path d="M308,0 C314.627417,-1.21743675e-15 320,5.372583 320,12 L320,468 C320,474.627417 314.627417,480 308,480 L12,480 C5.372583,480 8.11624501e-16,474.627417 0,468 L0,42.676 L42.676,0 L308,0 Z" id="path-1"></path>
            </defs>
            <g id="Symbols" stroke="none" strokeWidth="1" fill="white" fillRule="evenodd">
                <mask id="mask-2" fill="white">
                    <use></use>
                </mask>
                <path stroke={this.state.isSelected ? '#D91667' : '#1698D9'} strokeWidth="4" d="M308,2 C310.761424,2 313.261424,3.11928813 315.071068,4.92893219 C316.880712,6.73857625 318,9.23857625 318,12 L318,12 L318,468 C318,470.761424 316.880712,473.261424 315.071068,475.071068 C313.261424,476.880712 310.761424,478 308,478 L308,478 L12,478 C9.23857625,478 6.73857625,476.880712 4.92893219,475.071068 C3.11928813,473.261424 2,470.761424 2,468 L2,468 L2,43.5044271 L43.5044271,2 Z"></path>
            </g>
          </svg>
          {this.props.product.isDisabled && 
            <svg className='card__box-overlay-disabled' width="320px" height="480px" viewBox="0 0 320 480" version="1.1" xmlns="http://www.w3.org/2000/svg">
              <defs>
                  <path d="M308,0 C314.627417,-1.21743675e-15 320,5.372583 320,12 L320,468 C320,474.627417 314.627417,480 308,480 L12,480 C5.372583,480 8.11624501e-16,474.627417 0,468 L0,42.676 L42.676,0 L308,0 Z" id="path-1"></path>
              </defs>
              <g id="Symbols" stroke="none" strokeWidth="1" fill="#b3b3b3" fillOpacity='0.6' fillRule="evenodd">
                  <mask id="mask-2" fill="white">
                      <use></use>
                  </mask>
                  <path stroke='#b3b3b3' strokeWidth="4" d="M308,2 C310.761424,2 313.261424,3.11928813 315.071068,4.92893219 C316.880712,6.73857625 318,9.23857625 318,12 L318,12 L318,468 C318,470.761424 316.880712,473.261424 315.071068,475.071068 C313.261424,476.880712 310.761424,478 308,478 L308,478 L12,478 C9.23857625,478 6.73857625,476.880712 4.92893219,475.071068 C3.11928813,473.261424 2,470.761424 2,468 L2,468 L2,43.5044271 L43.5044271,2 Z"></path>
              </g>
          </svg>}
          <div className={'card__content ' + (this.props.product.isDisabled ? 'card__text_disabled' : '')}>
            <div className='card__info'>
              <div className='card__tagline'>
                {this.state.hoveredText && this.state.isSelected ? <span className='card__tagline_selected'>Котэ не одобряет?</span> : this.props.product.tagline}
              </div>
              <div className='card__product-name'>{this.props.product.productName}</div>
              <div className='card__flavour'>{this.props.product.flavour}</div>
              <div className='card__additional'>
                <div className='card__promoPortions'><span>{this.props.product.promoPortions}</span> порций</div>
                <div className='card__promoGifts'><span>{this.props.product.promoGifts}</span> {this.props.product.promoDeclination} в подарок</div>
                <div className='card__promoComments'>{this.props.product.promoComments}</div>
              </div>
            </div>
            <div className={'card__weight-info ' + 
                            (this.state.isSelected ? 'card__weight-info_selected' : '') + 
                            (this.props.product.isDisabled ? 'card__weight-info_disabled' : '')}>
              <span className='card__weight-info-count'>{this.props.product.weight}</span><span className='card__weight-info-units'>кг</span>
            </div>
            <img alt='' className='card__bg-image' src={bgImg} />
          </div>
        </div>
        <div className='card__additional-text'> 
          {!this.props.product.isDisabled && this.state.isSelected && <span>{this.props.product.additionalText}</span>}
          {!this.props.product.isDisabled && !this.state.isSelected && <span>Чего сидишь? Порадуй котэ, <span className='card__optional-select' onClick={this.selectProduct}>купи.</span></span>}
          {this.props.product.isDisabled && <span className='card__addtioanl-text_disabled'>Печалька, {this.props.product.flavour} закончился.</span>}
        </div>
      </div>
    );
  }
}

export default Card;
